require "lab42/basic_constraints"
BC = Lab42::BasicConstraints 
RSpec.describe Lab42::Arguments do
  class Positionals1 < Lab42::Arguments
    requires 0 # required
    allows_flags :dry_run, :verbose
  end

  context "description" do
    let(:desc) { private_data(Positionals1) }
    let(:allowed_positional_keywords) { desc.allowed_positional_keywords}
    let(:allowed_keywords) { desc.allowed_keywords}

    let(:expected_const) { {check: BC.bool, default: false} }
    
    it "allowed positional keywords are kept in a set..." do
      expect( allowed_positional_keywords ).to eq(Set.new(%i[dry_run verbose]))
    end

    it "...and they are described in the allowed_keywords hash" do
      dry_run_name = allowed_keywords[:dry_run][:check].name
      verbose_name = allowed_keywords[:verbose][:check].name
      expect( dry_run_name ).to eq(verbose_name)
    end
    
  end
end
