require "lab42/open_map/include"
module Support
  module DescriptionHelper
    def private_data from_arguments
      desc = from_arguments.send(:_description)
      hash =
      %i[
        aliases
        allowed_keywords
        required_keywords
        allowed_positional_keywords
        requireds
        allowed
      ].inject({}) do |result, ivar|
        result.update(ivar => desc.instance_variable_get("@#{ivar}"))
      end
      OpenMap.new(**hash)
    end
  end
end

RSpec.configure do |cfg|
  cfg.include Support::DescriptionHelper
end
