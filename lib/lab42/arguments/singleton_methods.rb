require_relative "description"

module Lab42
  class Arguments
    module SingletonMethods


      def allows argument_spec, constraint=nil, **keywords
        case argument_spec
        when Symbol
          _description.allow_keyword_argument argument_spec, constraint, keywords
        when Integer
          _description.allow_positional_argument argument_spec, constraint, keywords
        else
          raise ArgumentError, "argument_spec needs to be an Integer(indicating a positional arg) or a Symbol(indicating a keyword arg))"
        end
      end

      def allows_flags *flags
        flags.each { _description.allow_flag _1 }
      end


      def requires argument_spec, constraint=nil, **keywords
        case argument_spec
        when Symbol
          _description.require_keyword_argument argument_spec, constraint, keywords
        when Integer
          _description.require_positional_argument argument_spec, constraint, keywords
        else
          raise ArgumentError, "argument_spec needs to be an Integer(indicating a positional arg) or a Symbol(indicating a keyword arg))"
        end
      end
      
      private

      def _description
        @__description__ ||= Description.new(self)
      end
    end
  end
end
