Warning[:experimental] = false
require "lab42/basic_constraints"
module Lab42
  class Arguments
    class Description
      BC = Lab42::BasicConstraints
      InternalError = Class.new RuntimeError

      attr_reader  :allowed_keywords, :required_keywords

      def allow_flag pkwd
        raise ArgumentError, "duplicate definition of positional keyword #{pkwd}" if _defined_kwd?(pkwd) 
        # could also be seen as alias from a position to a keyword argument
        # e.g. if I can pass `..., :verbose, ...` or `..., verbose: true, ...` then 
        # `:verbose` shall be in `@allowed_positional_keywords` and in @allowed_keywors with the constraint `:bool` defaulting to `false`
        @allowed_positional_keywords << pkwd
        @allowed_keywords[pkwd] = {check: BC.bool, default: false}
      end

      def allow_keyword_argument name, type, kwds
        _add_keyword! name
        @allowed_keywords[name] = _make_constraint(type, kwds)
      end

      def allow_positional_argument idx, type, kwds
        raise ArgumentError, "positional required arguments need to be indicated in order, starting with 0" unless
          idx == @requireds.size + @allowed.size
        @allowed << _make_constraint(type, kwds)
      end

      def allowed_positional? current_size
        current_size <= @requireds.size + @allowed.size
      end

      def allowed_positional_keyword? kwd
        @allowed_positional_keywords.member? kwd
      end

      def check_positional! value, idx
        check = _all_positionals[idx][:check]
        return unless check
        case check.(value)
        in [:ok, nil]
        in [_, message]
          raise InvalidArgumentError, "@ position #{idx}: #{message}"
        end
      end

      def defaults_for_remaining current_size
        first_missing_idx = current_size - @requireds.size
        (first_missing_idx..@allowed.size)
          .map { _compute_positional_default _1 }
      end

      def require_keyword_argument argument_spec, constraint, kwds
        _add_keyword! argument_spec
        @required_keywords[argument_spec] = _make_constraint(constraint, kwds)
      end

      def require_positional_argument argument_spec, constraint, kwds
        raise ArgumentError, "positional required arguments need to be indicated in order, starting with 0" unless
          argument_spec == @requireds.size
        @requireds << _make_constraint(constraint, kwds)
      end

      def required_size
        @requireds.size
      end

      private


      def _add_keyword! kwd
        raise ArgumentError, "duplicate keyword definition for #{name.inspect}" if @defined_keywords.member?(kwd)
        @defined_keywords << kwd
      end

      def _all_positionals
        @requireds + @allowed
      end

      def _compute_positional_default idx
         case default = @allowed[idx]
         when nil
           nil
         when Hash
           default.fetch(:default) { default.fetch(:check) {nil}&.default }
         else
           raise InternalError, "unexpected positional arg description: #{default.inspect}"
         end
      end
        
      def _defined_kwd? kwd
        @allowed_keywords.has_key?(kwd)  || @required_keywords.has_key?(kwd) || @aliases.has_key?(kwd)
      end

      def _make_constraint(constraint, kwds)
        if Symbol === constraint
          kwds.merge(check: BC.from_symbol(constraint) )
        else
          kwds.merge(check: _make_constraint_value(kwds[:check]))
        end
      end

      def _make_constraint_value(from)
        case from
        when BC::Constraint
          from
        when Symbol
          BC.from_symbol(from)
        when NilClass
          nil
        else
          raise ArgumentError, "illegal value for :check #{constraint[:check].inspect}"
        end
      end

      def _total_size
        @___total_size__ ||= @requireds.size + @allowed.size
      end

      def initialize from_klass
        @klass = from_klass
        # Keywords
        @aliases = {}
        @allowed_keywords = {}
        @required_keywords   = {}
        # Positional Keywords, come after required positionals can be mixed with allowed positionals
        # for now, maybe we want to implement a strict version if there are use cases later.
        @defined_keywords = Set.new
        @allowed_positional_keywords = Set.new 
        # Positionals
        @requireds = []
        @allowed = []
      end
    end
  end
end
