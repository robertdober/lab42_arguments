module Lab42
  class Arguments
    InvalidArgumentError = Class.new RuntimeError
    MissingArgumentError = Class.new RuntimeError

    class Args


      private

      # Checks
      # ======
      def _assign_default key
        @keywords[key] = _compute_default(@all_keywords.fetch(key))
      end

      def _assign_defaults missing_keys
        missing_keys.each{ _assign_default _1 }
      end

      def _assign_positional_defaults
        @positionals += 
          @description
          .defaults_for_remaining @positionals.size
      end

      def _check_count!
        raise InvalidArgumentError, "too many positional arguments #{@positionals.inspect}" unless
        @description.allowed_positional?( @positionals.size )
      end

      def _check_keywords!
        missing_keys = @description.required_keywords.keys - @keywords.keys
        raise MissingArgumentError, "the following required keywords are missing: #{missing_keys.inspect}" unless missing_keys.empty?
        unprovided_optional_keys = @description.allowed_keywords.keys - @keywords.keys
        _assign_defaults unprovided_optional_keys
      end


      def _check_positionals!
        actual_size = @positionals.size
        required_size = @description.required_size
        raise MissingArgumentError, "missing required positional arguments, provided: #{actual_size}, required: #{required_size}" if
        actual_size < required_size
      end

      def _check_requirements!
        _check_positionals!
        _check_keywords!
      end

      def _compute_default desc
        return desc[:default] if desc.has_key?(:default)
        check = desc[:check]
        return check.default{nil} if check.respond_to? :default
      end

      # Parsing
      # =======
      def _parse_keyword key, value, description
        raise InvalidArgumentError, "keyword argument #{key.inspect} is not allowed" if description.nil?

      end

      def _parse_keywords keywords
        keywords.each do |key, value|
          _parse_keyword(key, value, @all_keywords[key]) 
        end
      end

      def _parse_positionals positionals
        positionals.each do | positional |
          case positional
          when Symbol
            _parse_positional_symbol positional
          else
            _parse_positional_anything positional
          end
        end
      end

      def _parse_positional_anything positional
        @description.check_positional!( positional, @positionals.size )
        @positionals << positional
        _check_count!
      end
      def _parse_positional_symbol positional
        if @description.allowed_positional_keyword?(positional)
          @keywords[positional] = true
        else
          _parse_positional_anything positional
        end
      end

      def initialize(description, *positionals, **keywords)
        @description = description.freeze
        @keywords    = keywords
        @positionals = []
        @all_keywords = @description.allowed_keywords.merge(@description.required_keywords)
        _parse_positionals positionals
        _assign_positional_defaults
        _parse_keywords keywords
        _check_requirements!
      end
    end
  end
end
