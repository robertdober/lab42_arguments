require_relative "arguments/args"
require_relative "arguments/singleton_methods"
module Lab42
  class Arguments

    attr_reader :args

    def self.inherited into
      into.extend SingletonMethods
    end


    def [] key, &blk
      _access_what(key)
        .fetch(key, &blk)
    end
    alias_method :fetch, :[]

    def keywords
      args.instance_variable_get("@keywords")
    end

    def positionals
      args.instance_variable_get("@positionals")
    end

    def valid?
      true
    end

    private

    def _access_what(key)
      case key
      when Integer
        positionals
      else
        keywords
      end
    end

    def initialize(*positionals, **keywords)
      @args = Args.new(self.class.instance_variable_get("@__description__"), *positionals, **keywords)
    end
  end
end
