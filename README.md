
[![Gem Version](https://badge.fury.io/rb/lab42_arguments.svg)](http://badge.fury.io/rb/lab42_arguments)

# Lab42::Arguments A DSL to easily define checks, requirements, interdependencies and aliases on arguments 

**N.B.** All these code examples are verified with [the speculate_about gem](https://rubygems.org/gems/speculate_about/)  

## Quick Starting Guide

Given a very primitive argument list for starters

```ruby
    Args = Lab42::Arguments
    class SimplestArgs < Args
      requires 0 # we need one positional unconstrained argument
    end
```

Then We can use it to parse a correct argument lists

```ruby
    some_args = SimplestArgs.new("hello")
    expect( some_args ).to be_valid
    expect( some_args.positionals ).to eq(%w[hello]) 
    expect( some_args[0] ).to eq("hello") 
    expect( some_args.keywords ).to be_empty
```

But if we forget the required argument
```ruby
    expect{ SimplestArgs.new() }.to raise_error(Args::MissingArgumentError, "missing required positional arguments, provided: 0, required: 1")
```

Given that we add some positional keyword arguments

```ruby
    class PositionalKwdArgsArgs < Args
      allows_flags :dry_run, :verbose
    end
```

Example: Adding additional keywords as positional parameters

```ruby
    some_args = PositionalKwdArgsArgs.new(:verbose)
    expect( some_args.positionals ).to be_empty
    expect( some_args.keywords ).to eq(dry_run: false, verbose: true)
```

And we can automatically provide them as keyword arguments if we want to do so

```ruby
    some_args = PositionalKwdArgsArgs.new(verbose: true)
    expect( some_args.keywords ).to eq(dry_run: false, verbose: true)
```

### Context Keywords

Given a little bit more complicated subclass
```ruby
    class SimplerArgs < Args
      requires 0, :string 
      allows :count, :positive_int
    end
```
Then we will notice that `:count` cannot be used as a positional_keyword anymore
```ruby
  expect{ SimplerArgs.new("hello", :count) }
    .to raise_error(Args::InvalidArgumentError, "illegal positional keyword argument :count")
```
And that `allows` does not, and thankfully so, require the presence of the keyword
```ruby
    expect{ SimplerArgs.new("hello") }.not_to raise_error
```

#### About Defaults

Given we extend our `SimplerArgs` with another optional keyword
```ruby
    class SimpleArgs < Args
      requires 0, :string 
      allows :count, :positive_int
      allows :legacy
    end
    let(:with_defaults) { SimpleArgs.new("with_defaults") }
```
Then we can demonstrate the difference of defaults depending on the fields type
```ruby
    expect( with_defaults[:count] ).to eq(1)
```

But untyped defaults are nil
```ruby
    expect( with_defaults.fetch(:legacy) ).to be_nil
```

#### Context Violated Constraints

##### Missing Arguments

Given a new class `MyArgs` 
```ruby
    class MyArgs < Args
      requires 0, :string
      requires :max, :non_negative_int, check: -> { _1 > 0} # of course we should use :positive_int instead
      allows :min, :non_negative_int, default: 1 
    end
```
Then we will quickly see that a first postional argument is needed
```ruby
    expect{ MyArgs.new(max: 1) }
      .to raise_error(Args::MissingArgumentError)
```
And also the `max` keyword
```ruby
    expect{ MyArgs.new("hello") }
      .to raise_error(Args::MissingArgumentError)
```

And the types need to be respected too
```ruby
    expect{ MyArgs.new(42, max: 1) }
      .to raise_error(Args::InvalidArgumentError, "@ position 0: 42 is not a legal string")
```

But symbols are interpreted differently
```ruby
    expect{ MyArgs.new(:hello, max: 1) }
      .to raise_error(Args::InvalidArgumentError, "illegal positional keyword argument :hello")
```

### Context More About Positionals

If we require let's say 3 positional arguments then these need to be present.
We have however seen before that they can come in later positions as positonal keywords (aka flags) might
be interspersed.

But sometimes we just want to allow optional positional parameters

Given a class with optional positonal arguments
```ruby
    class OptionalPositionals < Args
      requires 0, :positive_int
      allows 1  # → default = nil, check = nil
      allows 2, :number # <→ default = nil, check: number
      allows 3, :symbol, default: :default
    end
```

Then we can pass in only one argument, and all optional positionals will be defaulted
```ruby
    args = OptionalPositionals.new(1)
    expect( args.positionals ).to eq([1, nil, nil, :default])
```



# LICENSE
Copyright 2020 Robert Dober robert.dober@gmail.com

Apache-2.0 [c.f LICENSE](LICENSE)
