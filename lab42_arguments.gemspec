$:.unshift( File.expand_path( "../lib", __FILE__ ) )
require "lab42/arguments/version"
version = Lab42::Arguments::VERSION
Gem::Specification.new do |s|
  s.name        = 'lab42_arguments'
  s.version     = version
  s.summary     = 'Create A DSL that wraps and verifies complex argument logic'
  s.description = %{Create A DSL that wraps and verifies complex argument logic, with constraint checks, mutual dependencies, aliases and more}
  s.authors     = ["Robert Dober"]
  s.email       = 'robert.dober@gmail.com'
  s.files       = Dir.glob("lib/**/*.rb")
  s.files      += %w{LICENSE README.md}
  s.homepage    = "https://bitbucket.org/robertdober/lab42_arguments"
  s.licenses    = %w{Apache-2.0}

  s.required_ruby_version = '>= 2.7.0'
  s.add_dependency 'lab42_result', '~> 0.1.4'
  s.add_dependency 'lab42_basic_constraints', '~> 0.2.1'

  s.add_development_dependency 'pry', '~> 0.10'
  s.add_development_dependency 'pry-byebug', '~> 3.9'
  s.add_development_dependency 'rspec', '~> 3.10'
  # s.add_development_dependency 'travis-lint', '~> 2.0'
end
